import java.util.Scanner;
class SelectionSort{
	
	private static void selectionSort(int[] arr, int size){
		for(int i=0; i<arr.length-1; i++){
			int minIndex = i;
			for(int j=i+1; j<arr.length; j++){
				if(arr[minIndex] > arr[j]){
					minIndex = j;
				}
			}
			int temp = arr[i];
			arr[i] = arr[minIndex];
			arr[minIndex] = temp;
		}
	}
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array: ");
		int size = sc.nextInt();
		int[] arr = new int[size];
		System.out.println("Enter the elements of the array: ");
		for(int i=0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		selectionSort(arr,size);
		for(int i=0; i<arr.length; i++){
			System.out.print(arr[i]+" ");
		}
	}
}