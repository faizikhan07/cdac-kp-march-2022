import java.util.Scanner;
class BubbleSort{
	
	// Iterative Approach
	/*
	private static void bubbleSort(int[] arr, int size){
		for(int i=0; i<size-1; i++){
			for(int j=0; j<size-i-1; j++){
				if(arr[j] > arr[j+1]){
					int temp = arr[j+1];
					arr[j+1] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}
	*/
	// Recursive Approach
	private static void bubbleSort(int[] arr, int size){
		if(size == 1){
			return;
		}else{
			for(int i=0; i<size-1; i++){
				if(arr[i] > arr[i+1]){
					int temp = arr[i+1];
					arr[i+1] = arr[i];
					arr[i] = temp;
				}
			}
		}
		bubbleSort(arr,size-1);
	}
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array: ");
		int size = sc.nextInt();
		int[] arr = new int[size];
		System.out.println("Enter the elements of the array: ");
		for(int i=0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		bubbleSort(arr,size);
		for(int i=0; i<arr.length; i++){
			System.out.print(arr[i]+" ");
		}
	}
}