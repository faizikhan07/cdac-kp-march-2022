import java.util.Scanner;

class MergeSort {

	private static void conquer(int[] arr, int left, int right, int mid) {
		int length1 = (mid - left) + 1;
		int length2 = (right - mid);
		int[] L = new int[length1];
		int[] R = new int[length2];
		
		for(int i=0; i<length1; i++) 
			L[i] = arr[left-i];
		for(int i=0; i<length2; i++)
			R[i] = arr[mid+1+i];
		int i = 0, j = 0;
		 
        // Initial index of merged subarray array
        int k = left;
        while (i < length1 && j < length2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            }
            else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }
 
        while (i < length1) {
            arr[k] = L[i];
            i++;
            k++;
        }
 
        while (j < length2) {
            arr[k] = R[j];
            j++;
            k++;
        }
	}
	
	private static void divide(int[] arr, int left, int right) {
		if(left < right) {
			int mid = (left + right)/2;
			divide(arr,left,mid);
			
			divide(arr,mid+1,right);
			
			conquer(arr,left,right,mid);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array: ");
		int size = sc.nextInt();
		int[] arr = new int[size];
		System.out.println("Enter the elements of array: ");
		for(int i=0; i<arr.length; i++) {
			arr[i] = sc.nextInt();
		}
		divide(arr,0,arr.length-1);
		sc.close();
	}
}
