import java.util.Scanner;
public class MyStack{
	int[] arr;
	int top;
	int size;
	MyStack(int size){
		this.size = size;
		arr = new int[size];
		top = -1;
	}
	private boolean isFull(){
		return (top == size-1);
	}
	private void isEmpty(){
		if(top == -1){
			System.out.println("Stack Underflow");
		}else{
			System.out.println("Stack Overflow");
		}
	}
	private void push(int element){
		if(!isFull())
			arr[++top] = element;
		else
			System.out.println("Stack Overflow");
	}
	private void pop(){
		if(!isEmpty()){
			int temp = arr[top--];
			System.out.println("Element popped: "+temp);
		}else{
			System.out.println("Stack Underflow");
		}
	}
	private void peek(){
		if(isEmpty()){
			System.out.println("Stack Underflow");
		}else{
			System.out.println("Topmost element of the stack is: "+arr[top]);
		}
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the Stack: ");
		int size = sc.nextInt();
		MyStack stack = new MyStack(size);
		while(true){
		
			System.out.println("Enter your Choice: ");
			System.out.println("1. isEmpty: \n2. isFull: \n3. push: \n4. pop: \n5. peek: ");
			int choice = sc.nextInt();
			switch(choice){
				case 1:
					stack.isEmpty();
					break;
				case 2:
					if(stack.isFull())
						System.out.println("Stack is Full");
					else
						System.out.println("Stack is not Full");
					break;
				case 3:
					int element = sc.nextInt();
					stack.push(element);
					break;
				case 4:
					stack.pop();
					break;
				case 5:
					stack.peek();
					break;
				default:
					System.out.println("Enter the correct choice: ");
			}
		}
	}
}