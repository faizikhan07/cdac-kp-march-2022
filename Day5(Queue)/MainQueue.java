import java.util.Scanner;

class MyQueue{

	int[] arr;
	int size;
	int front, rear;

	MyQueue(int size){
		this.size = size;
		arr= new int[size];
		front = rear = -1;
	}

	boolean isFull(){
		return (rear == size-1 && front == 0);
	}

	boolean isEmpty(){
		return (rear == -1 && front == -1) || (front > rear);
	}

	void enque(int element){
		if(isFull()){
			System.out.println("Queue overflow..");
		}
		if(isEmpty()){
			front++;
			rear++;
			arr[rear] = element;
			System.out.println("Inserted elements is : "+ arr[rear]);
		}else{
			rear++;
			arr[rear] = element;
			System.out.println("Inserted elements is: "+arr[rear]);
		}
	}

	void deque(){
		if(isEmpty()){
			System.out.println("Queue underflow...");
		}else{
			int temp = arr[front++];
			System.out.println("We have delete element");
		}
	}
	
	void peek(){
		System.out.println("Topmost element of the Queue is: "+arr[rear]);
	}
}


class MainQueue{
	
	public static void main(String[] args){
		System.out.println("Enter the size of the array: ");
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		MyQueue mq = new MyQueue(size);
		mq.enque(10);
		mq.enque(20);
		mq.enque(30);
		mq.deque();
		if(mq.isFull()){
			System.out.println("True");
		}else{
			System.out.println("False");
		}
		if(mq.isEmpty()){
			System.out.println("True");
		}else{
			System.out.println("False");
		}
		mq.peek();
	}
}