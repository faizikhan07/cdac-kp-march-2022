import java.util.Scanner;
class BinarySearch{
	
	private static int binarySearch(int[] arr, int left, int right, int searchElement){
		while(left <= right){
			int mid = (left + right)/2;
			if(arr[mid] == searchElement){
				return mid;
			}
			else if(arr[mid] > searchElement){
				right = mid-1;
			}
			else{
				left = mid+1;
			}
		}
		return -1;
	}
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array: ");
		int size = sc.nextInt();
		int[] arr = new int[size];
		System.out.println("Enter the elements of the array: ");
		for(int i=0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter the element which you need to search in the array: ");
		int searchElement = sc.nextInt();
		int index = binarySearch(arr,0,arr.length-1,searchElement);
		if(index == -1){
			System.out.println("Element not found");
		}else{
			System.out.println("Element found at index: "+index);
		}
	}
}