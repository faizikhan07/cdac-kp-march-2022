import java.util.Scanner;
class LinearSearch{
	
	private static void linearSearch(int[] arr, int searchElement){
		int i=0,flag = 0;
		while(i<arr.length){
			if(arr[i] == searchElement){
				flag = 1;
				break;
			}
			i++;
		}
		if(flag == 1){
			System.out.println("Element found");
		}else{
			System.out.println("Element not found");
		}
	}
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array: ");
		int size = sc.nextInt();
		int[] arr = new int[size];
		System.out.println("Enter the elements of the array: ");
		for(int i=0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter the element which you need to search in the array: ");
		int searchElement = sc.nextInt();
		linearSearch(arr, searchElement);
	}
}